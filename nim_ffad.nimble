# Package

version       = "0.1.0"
author        = "Kaiyin Zhong"
description   = "Implement first order forward automatic differentiation"
license       = "MIT"
srcDir        = "src"
bin           = @["nim_ffad"]

# Dependencies

requires "nim >= 0.18.0"

task test, "Runs the test suite":
  exec "nim c -r tests/test1"