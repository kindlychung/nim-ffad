# First order forward automatic differentiation in Nim

A nim port of rust-ad. This is a so-called dual number implementation of autodiff, i.e. every operation is accompanied with a corresponding differentiation operation.

Have a look at the tests to see the magic at work. 


<!-- Tags: ad, autodiff, ml, ai, gradients -->
